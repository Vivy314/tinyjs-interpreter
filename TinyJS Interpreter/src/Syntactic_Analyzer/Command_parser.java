package Syntactic_Analyzer;

import static Syntactic_Analyzer.Expression_parser.parseExp;

import java.util.Collections;
import java.util.Vector;

import Exceptions.SyntaxException;
import Lexical_Analyzer.Tok;
import Lexical_Analyzer.Tokenizer;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.FormalParameter;
import Syntactic_Analyzer.ParseTree.FunArg;
import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.SimpleArg;
import Syntactic_Analyzer.ParseTree.StmtList;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Commands.AssignCom;
import Syntactic_Analyzer.ParseTree.Commands.BreakCom;
import Syntactic_Analyzer.ParseTree.Commands.Com;
import Syntactic_Analyzer.ParseTree.Commands.ContCom;
import Syntactic_Analyzer.ParseTree.Commands.DoCom;
import Syntactic_Analyzer.ParseTree.Commands.ForCom;
import Syntactic_Analyzer.ParseTree.Commands.IfCom;
import Syntactic_Analyzer.ParseTree.Commands.PrintCom;
import Syntactic_Analyzer.ParseTree.Commands.RetCom;
import Syntactic_Analyzer.ParseTree.Commands.SwitchCom;
import Syntactic_Analyzer.ParseTree.Commands.VarDecl;
import Syntactic_Analyzer.ParseTree.Commands.WhileCom;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Give static methods to parse commands statements of the languages.
 * 
 * @author Tommaso Catuogno
 *
 */
public class Command_parser {

	/**
	 * @param tok
	 *            input tokenizer from which to parse commands.
	 * @return A node of the syntax tree representing the parsed command is
	 *         returned.
	 */
	public static Com parseCom(Tokenizer tok) {
		// Manca quel cazzo di FOR
		Exp e;
		Block b;
		switch (tok.getToken().getFirst()) {
		case IF:

			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			e = parseExp(tok);
			tok.eat(Tok.C_R_BRACKET);
			// Typecheck probably

			b = parseBlock(tok, null);
			IfCom If = new IfCom(e, b);
			b = parseElse(tok);
			if (b != null)
				If.addBranch(b);
			return If;

		case DO:

			tok.eat();
			Parser.FLAG_LOOP = true;
			b = parseBlock(tok, null);
			Parser.FLAG_LOOP = false;
			tok.eat(Tok.WHILE);
			tok.eat(Tok.O_R_BRACKET);
			e = parseExp(tok);
			tok.eat(Tok.C_R_BRACKET);
			tok.eat(Tok.S_COLUMN);
			return new DoCom(e, b);

		case WHILE:

			tok.eat();// il while
			tok.eat(Tok.O_R_BRACKET);
			e = parseExp(tok);
			tok.eat(Tok.C_R_BRACKET);
			Parser.FLAG_LOOP = true;
			b = parseBlock(tok, null);
			Parser.FLAG_LOOP = false;
			return new WhileCom(e, b);

		case FOR:
			boolean decl = false;
			String str = null;
			AssignCom a1 = null,
			a3;
			VarDecl v1 = null;
			tok.eat();
			tok.eat(Tok.O_R_BRACKET);

			if (tok.getToken().getFirst() == Tok.VAR) {
				tok.eat();
				decl = true;
			}

			if (tok.getToken().getFirst() == Tok.IDE)
				str = (String) tok.getToken().getSecond();
			tok.eat(Tok.IDE);
			tok.eat(Tok.EQUAL);
			if (decl) {
				v1 = new VarDecl(str);
				v1.addValue(parseExp(tok));
			} else
				a1 = new AssignCom(str, parseExp(tok));

			tok.eat(Tok.S_COLUMN);

			e = parseExp(tok);
			tok.eat(Tok.S_COLUMN);

			if (tok.getToken().getFirst() == Tok.IDE)
				str = (String) tok.getToken().getSecond();
			tok.eat(Tok.IDE);
			tok.eat(Tok.EQUAL);
			a3 = new AssignCom(str, parseExp(tok));

			tok.eat(Tok.C_R_BRACKET);

			Parser.FLAG_LOOP = true;
			b = parseBlock(tok, null);
			Parser.FLAG_LOOP = false;

			if (decl)
				return new ForCom(v1, e, a3, b);
			else
				return new ForCom(a1, e, a3, b);

		case BREAK:
			if (!Parser.FLAG_LOOP)
				throw new SyntaxException("Break can be used only inside loops");
			tok.eat();
			tok.eat(Tok.S_COLUMN);
			return new BreakCom();

		case PRINT:
			Vector<Valuable> e1 = new Vector<Valuable>();
			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			e1.add(parseExp(tok));
			while (tok.getToken().getFirst() == Tok.COMMA) {
				tok.eat();
				e1.add(parseExp(tok));
			}

			tok.eat(Tok.C_R_BRACKET);
			tok.eat(Tok.S_COLUMN);
			return new PrintCom(e1);

		case VAR:
			tok.eat();
			tok.checkToken(Tok.IDE);
			VarDecl v = new VarDecl((String) tok.getToken().getSecond());
			tok.eat();

			if (tok.getToken().getFirst() == Tok.EQUAL) {
				tok.eat();

				if (tok.getToken().getFirst() == Tok.FUNCTION)
					v.addValue(parseFunDecl(tok));
				else {

					v.addValue(parseExp(tok));
					tok.eat(Tok.S_COLUMN);
				}
			} else {
				tok.eat(Tok.S_COLUMN);
				v.addValue(null);
			}
			return v;

		case IDE:

			AssignCom a;
			// Gestendo assegnamenti. Creato metodo in varname probabilmente
			// inutile.. serve una classe o no? mi sa di si.
			String name = (String) tok.getToken().getSecond();
			tok.setBackup();
			tok.eat();

			if (tok.getToken().getFirst() != Tok.EQUAL) {
				tok.restore();

				b = new Block(Expression_parser.parseExp(tok));
				tok.eat(Tok.S_COLUMN);
				return b;
			} else {
				tok.eat(Tok.EQUAL);
				if (tok.getToken().getFirst() == Tok.FUNCTION)
					a = new AssignCom(name, parseFunDecl(tok));
				else {
					a = new AssignCom(name, parseExp(tok));
					tok.eat(Tok.S_COLUMN);
				}
				return a;
			}

		case RETURN:

			tok.eat();
			e = parseExp(tok);
			tok.eat(Tok.S_COLUMN);
			return new RetCom(e);

		case CONTINUE:
			if (!Parser.FLAG_LOOP)
				throw new SyntaxException(
						"Continue command can be used only inside loops");
			tok.eat();
			tok.eat(Tok.S_COLUMN);
			return new ContCom();

		case SWITCH:

			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			e = parseExp(tok);
			tok.eat(Tok.C_R_BRACKET);
			tok.eat(Tok.O_C_BRACKET);

			SwitchCom s = parseCases(tok, new SwitchCom(e));
			if (!s.isdef)
				throw new SyntaxException("Default case must be declared");
			if (!s.isdefcase && !s.isdef)
				throw new SyntaxException(
						"At least one case have to be declared");
			tok.eat(Tok.C_C_BRACKET);
			return s;

			// Review
		default:
			throw new SyntaxException(tok.getToken());
		}
	}

	private static SwitchCom parseCases(Tokenizer tok, SwitchCom in) {
		SwitchCom s;
		switch (tok.getToken().getFirst()) {
		case CASE:
			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			tok.checkToken(Tok.NUM);
			int value = (int) tok.getToken().getSecond();
			tok.eat();
			tok.eat(Tok.C_R_BRACKET);
			tok.eat(Tok.COLUMN);
			in.addCases(value, parseBlock(tok, null));
			s = parseCases(tok, in);
			if (s == null)
				return in;
			else
				return s;

		case DEFAULT:
			tok.eat();
			tok.eat(Tok.COLUMN);
			in.addDefault(parseBlock(tok, null));
			s = parseCases(tok, in);
			if (s == null)
				return in;
			else
				return s;

		default:
			return null;
		}

	}

	private static Block parseElse(Tokenizer tok) {
		Block b;
		switch (tok.getToken().getFirst()) {
		case ELSE:
			tok.eat();
			b = parseBlock(tok, null);
			return b;
		default:
			return null;
		}

	}

	private static StmtList parseComList(Tokenizer tok) {
		StmtList c;
		StmtList s;

		switch (tok.getToken().getFirst().getGroup()) {
		case COM:
			c = new StmtList(parseCom(tok));
			s = parseComList(tok);
			if (s != null)
				c.addStmt(s);
			return c;
		case EXP:
			c = new StmtList(parseExp(tok));
			tok.eat(Tok.S_COLUMN);
			s = parseComList(tok);
			if (s != null)
				c.addStmt(s);
			return c;
		case IDE:
			c = new StmtList(parseCom(tok));
			s = parseComList(tok);
			if (s != null)
				c.addStmt(s);
			return c;

		default:
			return null;

		}
	}

	private static Block parseBlock(Tokenizer tok, Vector<FormalParameter> alist) {
		Block b;
		if (tok.getToken().getFirst() == Tok.O_C_BRACKET) {
			tok.eat();
			Parser.symbol_table.pushSymbolTable(alist);
			b = new Block(parseComList(tok));
			Parser.symbol_table.popSymbolTable();

			tok.eat(Tok.C_C_BRACKET);
			return b;
		}

		switch (tok.getToken().getFirst().getGroup()) {
		case COM:
			Parser.symbol_table.pushSymbolTable(alist);
			b = new Block(parseCom(tok));
			Parser.symbol_table.popSymbolTable();
			return b;

		case EXP:
			Parser.symbol_table.pushSymbolTable(alist);
			b = new Block(parseExp(tok));
			Parser.symbol_table.popSymbolTable();
			tok.eat(Tok.S_COLUMN);
			return b;

		case IDE:
			Parser.symbol_table.pushSymbolTable(alist);
			b = new Block(parseCom(tok));
			Parser.symbol_table.popSymbolTable();
			return b;

		default:
			throw new SyntaxException(tok.getToken());
		}
	}

	static FunExp parseFunDecl(Tokenizer tok) {
		String type;
		Block body;
		Vector<FormalParameter> alist;// = new Vector<FormalParameter>();
		switch (tok.getToken().getFirst()) {
		case FUNCTION:
			tok.eat();
			tok.checkToken(Tok.IDE);
			String name = (String) tok.getToken().getSecond();
			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			alist = parseArgList(tok);
			tok.eat(Tok.C_R_BRACKET);

			if (tok.getToken().getFirst() == Tok.COLUMN) {
				tok.eat(Tok.COLUMN);
				tok.checkToken(Tok.TYPE);
				type = (String) tok.getToken().getSecond();
				tok.eat();
			}
			// Case of procedure
			else
				type = "Undef";

			FunExp f = new FunExp(name, alist, type);
			if (tok.getToken().getFirst() != Tok.O_C_BRACKET)
				throw new SyntaxException(tok.getToken());

			Parser.FLAG_FUN = true;
			// Parser.symbol_table.pushSymbolTable(alist);
			body = parseBlock(tok, alist);
			// Parser.symbol_table.popSymbolTable();
			f.addBody(body);
			Parser.FLAG_FUN = false;
			return f;

		default:
			throw new SyntaxException(tok.getToken());
		}
	}

	private static Vector<FormalParameter> parseArgList(Tokenizer tok) {

		String type, id;
		Vector<FormalParameter> alist;
		FormalParameter var;
		FunArg f = null;
		boolean fun = false;
		switch (tok.getToken().getFirst()) {
		case IDE:
			type = null;
			// Vector<FormalParameter> tmp = new Vector<FormalParameter>();
			id = (String) tok.getToken().getSecond();
			tok.eat();

			if (tok.getToken().getFirst() == Tok.O_R_BRACKET) {
				fun = true;
				f = parse_functional(id, tok);
			}

			else {
				tok.eat(Tok.COLUMN);
				tok.checkToken(Tok.TYPE);
				type = (String) tok.getToken().getSecond();
				tok.eat();
			}

			alist = parseArgList1(tok);

			if (alist != null) {
				Collections.reverse(alist);
				if (!fun) {

					var = new SimpleArg(id, type);

					if (alist.contains(var))
						throw new SyntaxException(
								"Duplicated parameters in function");
					alist.add(var);
				} else {
					if (alist.contains(f))
						throw new SyntaxException(
								"Duplicated parameters in function");
					alist.add(f);
				}
			} else {
				alist = new Vector<FormalParameter>();
				if (!fun) {
					var = new SimpleArg(id, type);
					if (alist.contains(var))
						throw new SyntaxException("Duplicated parameter.");
					alist.add(var);
				}

				else {
					// var = new FunArg(id, type ,tmp);
					if (alist.contains(f))
						throw new SyntaxException("Duplicated parameter.");
					alist.add(f);
				}
			}
			Collections.reverse(alist);
			return alist;
		case C_R_BRACKET:
			return null;
		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static FunArg parse_functional(String fun_name, Tokenizer tok) {
		String type;
		Vector<FormalParameter> params = new Vector<FormalParameter>();

		tok.eat(Tok.O_R_BRACKET);

		while (tok.getToken().getFirst() != Tok.C_R_BRACKET) {

			// Simple Arg
			if (tok.getToken().getFirst() == Tok.TYPE) {
				params.add(new SimpleArg(null, (String) tok.getToken()
						.getSecond()));
				tok.eat();
			} else {
				if (tok.getToken().getFirst() == Tok.O_R_BRACKET)
					params.add(parse_functional(null, tok));
				else
					throw new SyntaxException(tok.getToken());
			}

			if (tok.getToken().getFirst() == Tok.COMMA)
				tok.eat();
			else if (tok.getToken().getFirst() != Tok.C_R_BRACKET)
				throw new SyntaxException(tok.getToken());
		}
		tok.eat();

		if (tok.getToken().getFirst() == Tok.COLUMN) {
			tok.eat();
			tok.checkToken(Tok.TYPE);
			type = (String) (tok.getToken().getSecond());
			tok.eat();
		} else
			type = "Undef";
		return new FunArg(fun_name, type, params);
	}

	private static Vector<FormalParameter> parseArgList1(Tokenizer tok) {
		switch (tok.getToken().getFirst()) {
		case COMMA:
			tok.eat();
			return parseArgList(tok);
		default:
			return null;
		}
	}
}
