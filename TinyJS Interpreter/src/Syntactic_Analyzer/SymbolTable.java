package Syntactic_Analyzer;

import java.util.Vector;

import Syntactic_Analyzer.ParseTree.FormalParameter;
import Syntactic_Analyzer.ParseTree.FunArg;
import Syntactic_Analyzer.ParseTree.SimpleArg;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Expression.IDE;

public class SymbolTable {
	private Vector<Tables> symbtab;
	private int index;

	SymbolTable() {
		symbtab = new Vector<Tables>();
		symbtab.add(new Tables(-1));
		index = 0;
	}

	public Valuable getVal(String in) {
		return symbtab.get(index).table.get(in);
	}

	public Valuable searchGet(String in1, int in2) {
		int i = in2;
		while (i >= 0)
			if (symbtab.get(i).table.get(in2) != null)
				return symbtab.get(i).table.get(in2);
			else
				i = symbtab.get(i).getStaticLink();
		return null;
	}

	public Valuable searchGet(String in) {
		int i = index;
		while (i >= 0)
			if (symbtab.get(i).table.get(in) != null)
				return symbtab.get(i).table.get(in);
			else
				i = symbtab.get(i).getStaticLink();
		return null;
	}

	public boolean searchSet(String in1, Valuable in2) {
		int i = index;
		while (i >= 0)
			if (symbtab.get(i).table.get(in1) != null) {
				symbtab.get(i).table.put(in1, in2);
				return true;
			} else
				i = symbtab.get(i).getStaticLink();
		return false;
	}

	public void setVal(String in1, Valuable in2) {
		symbtab.get(index).table.put(in1, in2);
	}

	public int Lookup(String in) {
		int i = index;
		while (i >= 0) {
			if (symbtab.get(i).table.get(in) != null)
				return i;
			else
				i = symbtab.get(i).getStaticLink();
		}
		return -1;
	}

	// public boolean LookupActual(String in){
	// return symbtab.get(index).table.containsKey(in);
	// }

	public String GetSymbType(String in) {
		Valuable tmp = getVal(in);
		if (tmp == null)
			return null;
		else
			return tmp.getType();
	}

	public void pushSymbolTable(Vector<FormalParameter> alist) {

		// Condition introduced just for nested high order call procedures
		// failure. Not sure about other cases.
		if (symbtab.get(index).ret != null)
			symbtab.get(index).ret = null;
		symbtab.add(new Tables(index));
		index = symbtab.size() - 1;

		if (alist != null)
			for (int i = 0; i < alist.size(); i++) {
				if (alist.get(i) instanceof FunArg) {
					symbtab.get(index).table.put(alist.get(i).id,
							new IDE(alist.get(i).id, (FunArg) alist.get(i)));
				} else
					symbtab.get(index).table.put(alist.get(i).id,
							new IDE(alist.get(i).id,
									((SimpleArg) alist.get(i)).types));
			}
	}

	public void popSymbolTable() {
		int index_old = index;
		index = symbtab.get(index).getStaticLink();

		// Nested block return.
		if (Parser.FLAG_FUN && symbtab.get(index_old).ret != null) {
			if (symbtab.get(index).ret != null)
				symbtab.get(index).ret.checkType(symbtab.get(index_old).ret
						.getType());
			symbtab.get(index).ret = symbtab.get(index_old).ret;
		}
	}

	public void setRetVal(Valuable in) {

		Valuable v = symbtab.get(index - 1).ret;
		if (v != null)
			v.checkType(in.getType());

		symbtab.get(symbtab.get(index).StaticLink).ret = in;
	}

	public Valuable getRetVal() {
		return symbtab.get(index).ret;
	}

	public int getTBLindex() {
		return index;
	}

	public int getStaticLink() {
		return symbtab.get(index).StaticLink;
	}
}