package Syntactic_Analyzer;

import Exceptions.SyntaxException;
import Lexical_Analyzer.Tok;
import Lexical_Analyzer.Tokenizer;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Additive;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Equalities;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Logical;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Multiplicative;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Relational;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;
import Syntactic_Analyzer.ParseTree.Commands.EvalCom;
import Syntactic_Analyzer.ParseTree.Expression.AddExp;
import Syntactic_Analyzer.ParseTree.Expression.AndExp;
import Syntactic_Analyzer.ParseTree.Expression.ApplyFun;
import Syntactic_Analyzer.ParseTree.Expression.BoolTerm;
import Syntactic_Analyzer.ParseTree.Expression.BracketTerm;
import Syntactic_Analyzer.ParseTree.Expression.CallArgs;
import Syntactic_Analyzer.ParseTree.Expression.EqExp;
import Syntactic_Analyzer.ParseTree.Expression.Exp;
import Syntactic_Analyzer.ParseTree.Expression.IDE;
import Syntactic_Analyzer.ParseTree.Expression.MinusTerm;
import Syntactic_Analyzer.ParseTree.Expression.MulExp;
import Syntactic_Analyzer.ParseTree.Expression.NotTerm;
import Syntactic_Analyzer.ParseTree.Expression.NumTerm;
import Syntactic_Analyzer.ParseTree.Expression.OrExp;
import Syntactic_Analyzer.ParseTree.Expression.RelExp;
import Syntactic_Analyzer.ParseTree.Expression.StringTerm;

/**
 * Give static methods to parse expression statements of the languages.
 * 
 * @author Tommaso Catuogno
 *
 */
public class Expression_parser {

	/**
	 * @param tok
	 *            input tokenizer from which to parse commands.
	 * @return A node of the syntax tree representing the parsed expression is
	 *         returned.
	 */
	public static Exp parseExp(Tokenizer tok) {
		Logical e1;
		OrExp e2;
		// System.out.println("Entered in Exp >" + tok.getToken().getFirst());

		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseAndExp(tok);
			e2 = parseOrExp(tok);
			if (e2 == null)
				return e1;
			else
				e2.addAndExp(e1);
			return e2;
		case IDE:
			e1 = parseAndExp(tok);
			e2 = parseOrExp(tok);
			if (e2 == null)
				return e1;
			else
				e2.addAndExp(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static OrExp parseOrExp(Tokenizer tok) {
		switch (tok.getToken().getFirst()) {
		case OR:
			tok.eat();

			return new OrExp(parseExp(tok));
		default:
			return null;
		}
	}

	private static Logical parseAndExp(Tokenizer tok) {
		Equalities e1;
		AndExp e2;
		// System.out.println("Entered in AndExp >" +
		// tok.getToken().getFirst());

		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseEqExp(tok);
			e2 = parseAndExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addEqExp(e1);
			return e2;

		case IDE:
			e1 = parseEqExp(tok);
			e2 = parseAndExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addEqExp(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static AndExp parseAndExp1(Tokenizer tok) {
		// System.out.println("Entered in AndExp1 >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst()) {
		case AND:
			tok.eat();
			return new AndExp(parseAndExp(tok), Tok.AND);

		default:
			return null;
		}
	}

	private static Equalities parseEqExp(Tokenizer tok) {
		Relational e1;
		EqExp e2;
		// System.out.println("Entered in EqExp >" + tok.getToken().getFirst());
		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseRelExp(tok);
			e2 = parseEqExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addRelExp(e1);
			return e2;

		case IDE:
			e1 = parseRelExp(tok);
			e2 = parseEqExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addRelExp(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static EqExp parseEqExp1(Tokenizer tok) {
		// System.out.println("Entered in EqExp1 >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst()) {
		case D_EQUAL:
			tok.eat();
			return new EqExp(parseEqExp(tok), Tok.EQUAL);

		case NOT:
			tok.eat();
			tok.eat(Tok.EQUAL);
			return new EqExp(parseEqExp(tok), Tok.NOTEQ);

		default:
			return null;
		}
	}

	private static Relational parseRelExp(Tokenizer tok) {
		Additive e1;
		RelExp e2;
		// System.out.println("Entered in RelExp >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseAddExp(tok);
			e2 = parseRelExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addAddExp(e1);
			return e2;

		case IDE:
			e1 = parseAddExp(tok);
			e2 = parseRelExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addAddExp(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static RelExp parseRelExp1(Tokenizer tok) {
		// System.out.println("Entered in RelExp1 >" +
		// tok.getToken().getFirst());

		switch (tok.getToken().getFirst()) {
		case LESS:
			tok.eat();

			if (tok.getToken().getFirst() == Tok.EQUAL) {
				tok.eat();
				return new RelExp(parseRelExp(tok), Tok.LESSEQ);
			} else
				return new RelExp(parseRelExp(tok), Tok.LESS);

			// Attento a cosa ritorni
		case GREATER:

			tok.eat();
			if (tok.getToken().getFirst() == Tok.EQUAL) {
				tok.eat();
				return new RelExp(parseRelExp(tok), Tok.GREATEREQ);
			} else
				return new RelExp(parseRelExp(tok), Tok.GREATER);

		default:
			return null;

		}
	}

	private static Additive parseAddExp(Tokenizer tok) {
		Multiplicative e1;
		AddExp e2;
		// System.out.println("Entered in AddExp >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseMulExp(tok);
			e2 = parseAddExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addMulExp(e1);
			return e2;

		case IDE:
			e1 = parseMulExp(tok);
			e2 = parseAddExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addMulExp(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static AddExp parseAddExp1(Tokenizer tok) {

		// System.out.println("Entered in AddExp1 >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst()) {
		case PLUS:
			tok.eat();
			return new AddExp(parseAddExp(tok), Tok.PLUS);

		case MINUS:
			tok.eat();
			return new AddExp(parseAddExp(tok), Tok.MINUS);

		default:
			return null;
		}

	}

	private static Multiplicative parseMulExp(Tokenizer tok) {

		Term e1;
		MulExp e2;
		// System.out.println("Entered in MulExp >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst().getGroup()) {
		case EXP:
			e1 = parseTerm(tok);
			e2 = parseMulExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addTerm(e1);
			return e2;

		case IDE:
			e1 = parseTerm(tok);
			e2 = parseMulExp1(tok);
			if (e2 == null)
				return e1;
			else
				e2.addTerm(e1);
			return e2;

		default:
			throw new SyntaxException(tok.getToken());
		}

	}

	private static MulExp parseMulExp1(Tokenizer tok) {
		// System.out.println("Entered in MulExp1 >" +
		// tok.getToken().getFirst());
		switch (tok.getToken().getFirst()) {
		case MUL:
			tok.eat();
			return new MulExp(parseMulExp(tok), Tok.MUL);

		case DIV:
			tok.eat();
			return new MulExp(parseMulExp(tok), Tok.DIV);

		default:
			return null;
		}
	}

	private static Term parseTerm(Tokenizer tok) {
		// System.out.println("Entered in Term >" + tok.getToken().getFirst());
		Exp e;

		switch (tok.getToken().getFirst()) {
		case O_R_BRACKET:
			tok.eat();
			e = parseExp(tok);
			tok.eat(Tok.C_R_BRACKET);
			return new BracketTerm(e);

		case MINUS:
			tok.eat();
			return new MinusTerm(parseExp(tok));

		case NOT:
			tok.eat();
			return new NotTerm(parseExp(tok));

		case IDE:

			String value = (String) tok.getToken().getSecond();
			tok.eat();
			// Function call case

			if (tok.getToken().getFirst() == Tok.O_R_BRACKET) {
				ApplyFun fun = new ApplyFun(value);

				tok.eat();

				CallArgs arg = parseCall(tok);

				if (arg != null)
					fun.addParam(arg);
				tok.eat(Tok.C_R_BRACKET);

				return fun;

			}

			else
				return new IDE(value);

		case EVAL:
			boolean distr = false;
			EvalCom eval;
			Exp e1 = null;
			// Potrebbe essere una variabile
			
			if ( tok.getToken().getSecond().equals("distributed"))
				distr=true;
			tok.eat();
			tok.eat(Tok.O_R_BRACKET);
			e = parseExp(tok);

			if (distr){
				
				tok.eat(Tok.COMMA);
				e1 = parseExp(tok);
				tok.eat(Tok.C_R_BRACKET);
			}
			else
				tok.eat(Tok.C_R_BRACKET);

			tok.setBackup();
			if (distr)
				eval = new EvalCom(e, e1);
			else
				eval = new EvalCom(e);
			tok.restore();
			return eval;

		case NUM:
			int i = (int) tok.getToken().getSecond();
			tok.eat();
			return new NumTerm(i);

		case STRING:
			String i1 = (String) tok.getToken().getSecond();
			tok.eat();
			
			return new StringTerm(i1.substring(1, i1.length() - 1));

		case BOOL:
			boolean i11 = Boolean.parseBoolean((String) tok.getToken()
					.getSecond());
			tok.eat();

			return new BoolTerm(i11);
		default:
			throw new SyntaxException(tok.getToken());

		}

	}

	private static CallArgs parseCall(Tokenizer tok) {
		CallArgs arg = new CallArgs();
		switch (tok.getToken().getFirst()) {
		case C_R_BRACKET:
			return null;
		default:
			arg.addArg(parseExp(tok));
			parseMoreCallArg(tok, arg);
		}
		// Controlla
		return arg;
	}

	private static CallArgs parseMoreCallArg(Tokenizer tok, CallArgs in) {
		CallArgs tmp;
		switch (tok.getToken().getFirst()) {
		case COMMA:
			tok.eat();
			in.addArg(parseExp(tok));
			tmp = parseMoreCallArg(tok, in);
			if (tmp == null)
				return in;
			else
				return tmp;

		default:
			return null;

		}
	}
}
