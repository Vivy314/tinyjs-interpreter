package Syntactic_Analyzer;

import java.util.Hashtable;

/**
 * Activation_record represent one of the structure for the run time support
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class Activation_Record {
	public Hashtable<String, Object> Values;
	// public int ST_link, dynamic_link;
	public Object retVal;

	public Activation_Record(Hashtable<String, Object> in3) {
		if (in3 == null)
			Values = new Hashtable<String, Object>();
		else
			Values = in3;

		retVal = null;
	}
}