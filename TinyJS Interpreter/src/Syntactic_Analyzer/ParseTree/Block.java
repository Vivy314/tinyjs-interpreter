package Syntactic_Analyzer.ParseTree;

import java.util.Hashtable;

import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Commands.Com;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing the Block statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class Block extends Com {

	Com par1;
	Exp par2;
	StmtList par3;
	String ret_type = null;
	Hashtable<String, Object> env = null;

	public Block(Com in) {

		par1 = in;
	}

	public Block(Exp in) {
		par2 = in;
		ret_type = par2.getType();
	}

	public Block(StmtList in) {
		par3 = in;
	}

	public String getType() {
		return ret_type;
	}

	/**
	 * Return a string representing the actual object.
	 */
	@Override
	public String toString() {
		if (par1 != null)
			return par1.toString();
		if (par2 != null)
			return par2.toString();
		if (par3 != null && !Parser.FLAG_RET)
			return "{ " + par3.toString() + " }";
		return "{}";
	}

	/**
	 * Method used to evaluate and execute the actual object.
	 */
	public void setEnv(Hashtable<String, Object> in) {
		env = in;
	}

	@Override
	public Object eval() {
		Object ret = null;
		Parser.stack.pushStack(env);
		
		if (par1 != null)
			ret = par1.eval();
		if (par2 != null)
			ret = par2.eval();
		if (par3 != null)
			ret = par3.eval();
		Parser.stack.popStack();
		return ret;
	}
}