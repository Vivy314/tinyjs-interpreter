package Syntactic_Analyzer.ParseTree.Expression;

import java.util.Vector;

/**
 * Container class for actual parameters of a call.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class CallArgs {
	Vector<Exp> args;

	public CallArgs() {
		args = new Vector<Exp>();
	}

	public void addArg(Exp in) {
		args.add(in);
	}

	@Override
	public String toString() {
		String ret = "( ";
		for (int i = 0; i < args.size(); i++) {
			ret += args.get(i).toString();
			if (i < args.size() - 1)
				ret += ", ";
		}
		return ret + " )";
	}

}
