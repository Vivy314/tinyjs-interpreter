package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.FunArg;
import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing an identifier.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class IDE extends Term {
	String name, type;
	FunArg params_type;

	/**
	 * 
	 * @param in
	 *            Name of the identifier.
	 */
	public IDE(String in) {

		name = in;
		if (Parser.symbol_table.Lookup(name) == -1)
			throw new SyntaxException("Variable " + in
					+ " not declared in that scope.", LINE);

		type = Parser.symbol_table.searchGet(in).getType();
	}

	/**
	 * 
	 * @param in1
	 *            Name of the identifier.
	 * @param in2
	 *            Type of the identifier.
	 */

	public IDE(String in1, String in2) {

		name = in1;
		type = in2;
	}

	/**
	 * 
	 * @param in1
	 *            Name of the functional identifier.
	 * @param in2
	 *            Vector containing types for parameters and returned value.
	 */
	public IDE(String in1, FunArg in2) {
		name = in1;
		params_type = in2;
		type = "Fun";
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String getType() {
		return type;
	}

	public FunArg getFunTypes() {
		return params_type;
	}

	@Override
	public void checkType(String in) {
		if (!in.equals(this.getType()))
			throw new TypeException(this.getType(), in);
	}

	@Override
	public Object eval() {

		Object v = Parser.stack.searchGet(name, LINE);
		if (v instanceof FunExp) {
		}
		return v;
	}

}