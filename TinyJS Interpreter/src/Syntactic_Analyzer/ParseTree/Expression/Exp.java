package Syntactic_Analyzer.ParseTree.Expression;

import Syntactic_Analyzer.ParseTree.Valuable;

/**
 * Node of the Syntax Tree representing an Expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public abstract class Exp extends Valuable {
	AndExp e1;
	OrExp e2;

	@Override
	public abstract Object eval();

	@Override
	public abstract String getType();

	@Override
	public abstract void checkType(String in);

	@Override
	public String toString() {
		if (e2 == null)
			return e1.toString();
		else
			return e1.toString() + e2.toString();
	}
}