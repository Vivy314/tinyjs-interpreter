package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Lexical_Analyzer.Tok;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Multiplicative;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a multiplicative expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class MulExp extends Multiplicative {

	Term term;
	Multiplicative Moreterm;
	Tok OP;

	public MulExp(Multiplicative in1, Tok in2) {

		Moreterm = in1;
		Moreterm.checkType("Int");
		OP = in2;
	}

	public void addTerm(Term in) {
		term = in;
		term.checkType("Int");
	}

	@Override
	public String toString() {
		switch (OP) {
		case MUL:
			return term.toString() + " * " + Moreterm.toString();
		case DIV:
			return term.toString() + " / " + Moreterm.toString();
		default:
			throw new SyntaxException("!", LINE);
		}
	}

	@Override
	public String getType() {

		term.checkType("Int");
		Moreterm.checkType("Int");
		return "Int";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Int"))
			throw new TypeException(in, "Int");
	}

	@Override
	public Integer eval() {
		if (OP == Tok.MUL)
			return (Integer) term.eval() * (Integer) Moreterm.eval();
		else
			return (Integer) term.eval() / (Integer) Moreterm.eval();
	}
}