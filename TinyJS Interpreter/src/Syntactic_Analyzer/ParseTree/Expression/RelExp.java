package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Lexical_Analyzer.Tok;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Additive;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Relational;

/**
 * Node of the Syntax Tree representing a relational expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class RelExp extends Relational {
	Additive e1;
	Relational e2;
	Tok OP;

	public RelExp(Relational in, Tok in2) {
		e1 = null;
		e2 = in;
		OP = in2;
		e2.checkType("Int");

	}

	public void addAddExp(Additive in) {
		e1 = in;
		e1.checkType("Int");
	}

	@Override
	public String toString() {
		switch (OP) {

		case LESS:
			return e1.toString() + " < " + e2.toString();
		case LESSEQ:
			return e1.toString() + " <= " + e2.toString();
		case GREATER:
			return e1.toString() + " > " + e2.toString();
		case GREATEREQ:
			return e1.toString() + " >= " + e2.toString();
		default:
			throw new SyntaxException("Wrong operand", LINE);
		}
	}

	@Override
	public String getType() {
		e1.checkType("Int");
		e2.checkType("Int");
		return "Bool";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");
	}

	@Override
	public Object eval() {
		switch (OP) {

		case LESS:
			return (Integer) e1.eval() < (Integer) e2.eval();
		case LESSEQ:
			return (Integer) e1.eval() <= (Integer) e2.eval();
		case GREATER:
			return (Integer) e1.eval() > (Integer) e2.eval();
		case GREATEREQ:
			return (Integer) e1.eval() >= (Integer) e2.eval();
		default:
			throw new TypeException("", "");
		}
	}
}