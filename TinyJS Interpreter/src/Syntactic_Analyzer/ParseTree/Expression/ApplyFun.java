package Syntactic_Analyzer.ParseTree.Expression;

import java.util.Hashtable;
import java.util.Vector;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.FormalParameter;
import Syntactic_Analyzer.ParseTree.FunArg;
import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.SimpleArg;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing the function call statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class ApplyFun extends Term {
	// Name of the Calling function.
	public String name;
	// List of actual parameters. That include also their type and
	Vector<Valuable> Act_params;

	// Formal types and return value type taken from the function
	Vector<FormalParameter> types;
	String rettype;

	private int size, size2;
	FunExp fun;

	/**
	 * 
	 * @param in1
	 *            Name of the function we are calling.
	 */
	public ApplyFun(String in1) {
		name = in1;
		Act_params = new Vector<Valuable>();

		if (Parser.symbol_table.Lookup(name) == -1)
			throw new SyntaxException("Function name " + in1 + " not declared",
					LINE);
		Parser.symbol_table.searchGet(name).checkType("Fun");

		// Case of parametric functions, we actually know just the signature of
		// the function.
		if (Parser.symbol_table.searchGet(name) instanceof IDE) {
			IDE tmp1 = (IDE) Parser.symbol_table.searchGet(name);
			types = tmp1.getFunTypes().types;
			rettype = tmp1.getFunTypes().rettype;
		} else {
			if (Parser.symbol_table.searchGet(name) instanceof ApplyFun)
				fun = ((ApplyFun) Parser.symbol_table.searchGet(name)).fun;
			else
				fun = (FunExp) Parser.symbol_table.searchGet(name);
			types = fun.arglist;
			rettype = fun.rettype;
		}

	}

	public void addParam(CallArgs in) {

		size = in.args.size();
		size2 = types.size();

		if (size > size2)
			{
			System.out.println();
			throw new SyntaxException("Too many parameters while calling function " + name + ".");
			}

		// checking formal parameter's type against actual's ones.
		for (int i = 0; i < size; i++) {
			Act_params.add(in.args.get(i));
			if (types.get(i) instanceof SimpleArg)
				((SimpleArg) types.get(i)).equal_type(Act_params.get(i)
						.getType());

			else {

				if (!Act_params.get(i).getType().equals("Fun"))
					throw new TypeException(Act_params.get(i).getType(), types
							.get(i).toString());
				// Instanciating formal function with the actual's one.

				Act_params.set(i, Parser.symbol_table.searchGet(Act_params.get(
						i).toString()));
				if (Act_params.get(i) instanceof IDE)
					((FunArg) types.get(i))
							.equal_type(((IDE) Act_params.get(i)).getFunTypes());
				else
					((FunArg) types.get(i)).equal_type((FunExp) Act_params
							.get(i));
			}
		}
	}

	@Override
	public Object eval() {
		Hashtable<String, Object> env = new Hashtable<String, Object>();
		// Case of high order function.
		if (fun == null) {
			Object o = Parser.stack.getValue(name);

			if ((o == null) || !(o instanceof FunExp))
				throw new SyntaxException(name, LINE);
			fun = (FunExp) Parser.stack.getValue(name);
		}

		for (int i = 0; i < size2; i++) {
			if (i < size) {
				Valuable v = Act_params.get(i);
				if (v.getType() == "Fun") {
					if (v instanceof ApplyFun)

						env.put(fun.arglist.get(i).id, v.eval());
					else
						env.put(fun.arglist.get(i).id, v);
				} else {
					if (v.getType() != "Fun" || v instanceof IDE)
						env.put(fun.arglist.get(i).id, v.eval());
					else
						env.put(fun.arglist.get(i).id, v);
				}
			} else
				env.put(fun.arglist.get(i).id, "Undef");
		}

		Block b = fun.body;
		b.setEnv(env);
		b.eval();
		Parser.FLAG_RET = false;

		return Parser.stack.getRetVal();
	}

	@Override
	public String getType() {
		return rettype;
	}

	@Override
	public void checkType(String in) {

		if (!in.equals(rettype))
			throw new TypeException(fun.rettype, in);
	}

	@Override
	public String toString() {
		String ret = name + "(";
		for (int i = 0; i < Act_params.size(); i++) {
			ret += Act_params.get(i).toString();
			if (i < Act_params.size() - 1)
				ret += ", ";
		}
		return ret += ")";
	}
}