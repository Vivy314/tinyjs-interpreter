package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a string expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class StringTerm extends Term {
	String value;

	public StringTerm(String in) {
		value = in;
	}

	@Override
	public String toString() {
		return "\"" + value + "\"";
	}

	@Override
	public String getType() {
		return "String";

	}

	public String Eval() {
		return value;
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("String"))
			throw new TypeException(in, "String");
	}

	@Override
	public String eval() {
		return value;
	}
}