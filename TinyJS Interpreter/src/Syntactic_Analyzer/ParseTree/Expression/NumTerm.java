package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a constant number.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class NumTerm extends Term {

	int par;

	public NumTerm(int in) {
		par = in;
	}

	@Override
	public String toString() {
		return String.valueOf(par);
	}

	@Override
	public String getType() {
		return "Int";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Int"))
			throw new TypeException(in, "Int");
	}

	@Override
	public Integer eval() {
		return par;
	}

}
