package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Lexical_Analyzer.Tok;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Equalities;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Logical;

/**
 * Node of the Syntax Tree representing the logical AND expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class AndExp extends Logical {
	Equalities e1;
	Logical e2;
	Tok OP;

	public AndExp(Logical in1, Tok in2) {
		e1 = null;
		e2 = in1;
		OP = in2;
		e2.checkType("Bool");

	}

	public void addEqExp(Equalities in) {
		e1 = in;
		e1.checkType("Bool");
	}

	@Override
	public String toString() {
		return e1.toString() + " && " + e2.toString();
	}

	@Override
	public String getType() {
		e1.checkType("Bool");
		e2.checkType("Bool");
		return "Bool";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");
	}

	@Override
	public Boolean eval() {
		return (Boolean) e1.eval() && (Boolean) e2.eval();
	}
}
