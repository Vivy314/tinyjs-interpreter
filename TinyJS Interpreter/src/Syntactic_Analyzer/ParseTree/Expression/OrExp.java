package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Logical;

/**
 * Node of the Syntax Tree a logical OR expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class OrExp extends Exp {
	Logical e1;
	Exp e2;

	public OrExp(Exp in) {
		e2 = in;
		e2.checkType("Bool");
	}

	public void addAndExp(Logical in) {
		e1 = in;
		e1.checkType("Bool");

	}

	@Override
	public String toString() {
		return e1.toString() + " || " + e2.toString();
	}

	@Override
	public String getType() {
		e1.checkType("Bool");
		e2.checkType("Bool");
		return "Bool";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");
	}

	@Override
	public Boolean eval() {
		return (Boolean) e1.eval() || (Boolean) e2.eval();
	}

}