package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Lexical_Analyzer.Tok;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Additive;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Multiplicative;

/**
 * Node of the Syntax Tree representing the addition or subtraction expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class AddExp extends Additive {

	Multiplicative e1;
	Additive e2;
	Tok OP;

	public AddExp(Additive in1, Tok in2) {
		e1 = null;
		e2 = in1;
		OP = in2;
		e2.checkType("Int");
	}

	public void addMulExp(Multiplicative in) {
		e1 = in;
		e1.checkType("Int");
	}

	@Override
	public String toString() {

		switch (OP) {
		case PLUS:
			return e1.toString() + " + " + e2.toString();
		case MINUS:
			return e1.toString() + " - " + e2.toString();
		default:
			throw new SyntaxException("!", LINE);
		}

	}

	@Override
	public String getType() {

		e1.checkType("Int");
		e2.checkType("Int");
		return "Int";
	}

	@Override
	public void checkType(String in) {

		if (!in.equals("Int"))
			throw new TypeException(in, "Int");
	}

	@Override
	public Object eval() {
		if (OP == Tok.PLUS)
			return (Integer) e1.eval() + (Integer) e2.eval();
		else
			return (Integer) e1.eval() - (Integer) e2.eval();

	}
}