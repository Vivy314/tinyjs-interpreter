package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Lexical_Analyzer.Tok;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Equalities;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Relational;

/**
 * Node of the Syntax Tree representing the equal and not equal expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class EqExp extends Equalities {
	Relational e1;
	Equalities e2;
	Tok OP;

	public EqExp(Equalities in1, Tok in2) {
		e2 = in1;
		OP = in2;
	}

	public void addRelExp(Relational in) {
		e1 = in;
		if (e1.getType() != e2.getType())
			throw new TypeException(e1.getType(), e2.getType());
	}

	@Override
	public String toString() {
		switch (OP) {
		case EQUAL:
			return e1.toString() + " == " + e2.toString();
		case NOTEQ:
			return e1.toString() + " != " + e2.toString();
		default:
			throw new SyntaxException("Wrong argument", LINE);
		}

	}

	@Override
	public String getType() {
		if ((e1.getType() == e2.getType()))
			return "Bool";
		else
			throw new TypeException(e1.getType(), e2.getType());
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");
	}

	@Override
	public Boolean eval() {
		return e1.eval().equals(e2.eval());
	}
}
