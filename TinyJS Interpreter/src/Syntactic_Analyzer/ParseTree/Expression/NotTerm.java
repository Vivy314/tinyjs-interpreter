package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a negated expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class NotTerm extends Term {
	Exp p1;

	public NotTerm(Exp in1) {
		p1 = in1;
		p1.checkType("Bool");
	}

	@Override
	public String toString() {
		return "!" + p1.toString();
	}

	@Override
	public String getType() {
		p1.checkType("Bool");
		return "Bool";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");
	}

	@Override
	public Boolean eval() {
		return !(Boolean) p1.eval();
	}

}
