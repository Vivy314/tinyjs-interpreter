package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a boolean term.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class BoolTerm extends Term {

	boolean par;

	public BoolTerm(boolean in) {
		par = in;
	}

	@Override
	public Boolean eval() {
		return par;
	}

	@Override
	public String toString() {
		return String.valueOf(par);
	}

	@Override
	public String getType() {
		return "Bool";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Bool"))
			throw new TypeException(in, "Bool");

	}

}