package Syntactic_Analyzer.ParseTree.Expression;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing an expression preceded by minus.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class MinusTerm extends Term {
	Exp p1;

	public MinusTerm(Exp in1) {
		p1 = in1;
		p1.checkType("Int");
	}

	@Override
	public String toString() {
		return "-" + p1.toString();
	}

	@Override
	public String getType() {
		p1.checkType("Int");
		return "Int";
	}

	@Override
	public void checkType(String in) {
		if (!in.equals("Int"))
			throw new TypeException(in, "Int");

	}

	@Override
	public Integer eval() {
		return -(Integer) p1.eval();
	}

}
