package Syntactic_Analyzer.ParseTree.Expression;

import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;

/**
 * Node of the Syntax Tree representing a bracket expression.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class BracketTerm extends Term {
	Exp p1;

	public BracketTerm(Exp in1) {
		p1 = in1;
	}

	@Override
	public String toString() {
		return "(" + p1.toString() + ")";
	}

	@Override
	public String getType() {
		return p1.getType();
	}

	@Override
	public void checkType(String in) {
		p1.checkType(in);
	}

	@Override
	public Object eval() {
		return (p1.eval());
	}

}
