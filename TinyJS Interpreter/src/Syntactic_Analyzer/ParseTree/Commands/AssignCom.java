package Syntactic_Analyzer.ParseTree.Commands;

import Exceptions.SyntaxException;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Expression.ApplyFun;
import Syntactic_Analyzer.ParseTree.Expression.IDE;

/**
 * Node of the Syntax Tree representing the assignment command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class AssignCom extends Com {
	String id;
	Valuable value;

	public AssignCom(String in1, Valuable in2) {
		id = in1;
		value = in2;

		if (in2 instanceof ApplyFun && (in2.getType() == "Undef"))
			throw new SyntaxException(((ApplyFun) in2).name
					+ " is a procedure and cannot be assigned", LINE);

		boolean NOT_INSTANCIATED = false;
		if (Parser.symbol_table.Lookup(in1) == -1)
			throw new SyntaxException("Variable " + in1
					+ " not declared in that scope.", LINE);

		while (value instanceof IDE && !NOT_INSTANCIATED) {
			if (Parser.symbol_table.searchGet(value.toString()).toString() == value
					.toString())
				NOT_INSTANCIATED = true;
			value = Parser.symbol_table.searchGet(value.toString());
		}
		if (value.getType() == "Fun"
				&& Parser.symbol_table.getStaticLink() != -1
				&& !NOT_INSTANCIATED)
			throw new SyntaxException(
					"Nested function declaration not allowed.", LINE);

		Parser.symbol_table.searchSet(in1, value);

	}

	@Override
	public Object eval() {

		if (value.getType() != "Fun" || value instanceof IDE)
			Parser.stack.searchSet(id, value.eval());
		else
			Parser.stack.searchSet(id, value);
		return null;
	}

	@Override
	public String toString() {
		return id + " = " + value.toString() + ";";
	}

}