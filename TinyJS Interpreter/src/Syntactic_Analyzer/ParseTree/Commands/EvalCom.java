package Syntactic_Analyzer.ParseTree.Commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Lexical_Analyzer.Tokenizer;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.Abastract_Nodes.Term;
import Syntactic_Analyzer.ParseTree.Expression.Exp;
import Syntactic_Analyzer.ParseTree.Expression.IDE;

/**
 * Node of the Syntax Tree representing the evaluation command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class EvalCom extends Term {
	Exp e, u;
	String s, type;
	Parser p;

	public EvalCom(Exp in) {
		in.checkType("String");
		e = in;
		init();
	}

	public EvalCom(Exp in, Exp in2) {
		in.checkType("String");
		in2.checkType("String");
		e = in;
		u = in2;
		init();
	}

	private void init() {
		while (e instanceof IDE)
			e = (Exp) Parser.symbol_table.searchGet(e.toString());
		System.out.println("PARSER new "  + e.toString());
		Tokenizer t = new Tokenizer(e.toString().substring(1,
				e.toString().length() - 1));

		p = new Parser(t, Parser.symbol_table);
		p.Parse();
		type = "RTcheck";

	}

	@Override
	public Object eval() {
		// Distributed eval if u != null
		if (u != null) {
			try {
				
				
				/*URL url = new URL(u.eval().toString());
				
					URLConnection urlConnection = url.openConnection();
				urlConnection.setDoOutput(true);
				Pattern p = Pattern.compile("(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?");  

			    Matcher m = p.matcher(url.toString());
				if (!m.matches() ) throw new RuntimeException("Url not well formatted");
			    
				PrintWriter writer = new PrintWriter(
						urlConnection.getOutputStream());
				*/
				//To be used with URL, just change the lines above with the one commented
				Socket sock = new Socket("localhost",60170 );
				PrintWriter writer = new PrintWriter(sock.getOutputStream());
				
				System.out.println("CLIENT: invio di codice:" + e.toString());
				
				writer.println(e.eval());
				writer.flush();
			//	writer.close();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(sock.getInputStream()));
				String output = reader.readLine();
				System.out.println("CLIENT: ricevuto da socket:" + output);
				sock.close();
				
				return output;
			} catch (Exception e) {
				throw new SyntaxException("Connection to the server failed");
			}
		}
		else {
			s = (String) e.eval();
			p.AddStack(Parser.stack);
			Object o = p.Execute();
			if (o instanceof String)
				type = "String";
			if (o instanceof Integer)
				type = "Int";
			if (o instanceof Boolean)
				type = "Bool";
			if (o instanceof FunExp)
				type = "Fun";
			return o;
		}
	}

	@Override
	public String toString() {
		if (u == null)
			return "eval(" + s + ");";
		else
			return "distribute_eval( " + s + " , " + u + ");";
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void checkType(String in) {
		if (!in.equals(type))
			throw new TypeException(in, type);
	}
}