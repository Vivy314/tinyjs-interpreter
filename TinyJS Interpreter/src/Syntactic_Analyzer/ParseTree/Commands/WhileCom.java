package Syntactic_Analyzer.ParseTree.Commands;

import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing the while loop Statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class WhileCom extends Com {
	Block b;
	Exp e;

	public WhileCom(Exp in1, Block in2) {
		e = in1;
		e.checkType("Bool");
		b = in2;

	}

	@Override
	public Object eval() {
		while ((Boolean) e.eval()) {
			b.eval();
			if (Parser.FLAG_BREAK) {
				Parser.FLAG_BREAK = false;
				break;
			}
			if (Parser.FLAG_CONTINUE)
				Parser.FLAG_CONTINUE = false;
		}
		return null;
	}

	@Override
	public String toString() {
		return "while(" + e.toString() + ") " + b.toString();
	}

}
