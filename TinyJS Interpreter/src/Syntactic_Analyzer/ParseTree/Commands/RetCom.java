package Syntactic_Analyzer.ParseTree.Commands;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.Valuable;

/**
 * Node of the Syntax Tree representing the return command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class RetCom extends Com {
	Valuable e;

	public RetCom(Valuable in) {
		if (!Parser.FLAG_FUN)
			throw new SyntaxException(
					"return expected inside functions declaration", LINE);
		e = in;
		if (Parser.symbol_table.getRetVal() != null
				&& Parser.symbol_table.getRetVal().getType() != e.getType())
			throw new TypeException(
					"Missmatch among returned value and expected one."
							+ Parser.symbol_table.getRetVal().getType(), LINE);

		Parser.symbol_table.setRetVal(e);
		Parser.FLAG_RET = true;

	}

	@Override
	public Object eval() {
		Object ret = null;
		if (!(e instanceof FunExp)) {
			ret = e.eval();

//			System.out.println("SONO IN RET >" + ret.toString()+"  "
//			 +Parser.stack.index);
			Parser.stack.setRetVal(ret);
		}
		Parser.FLAG_RET = true;
		return ret;
	}

	@Override
	public String toString() {
		return "return " + e.toString() + ";";
	}
}