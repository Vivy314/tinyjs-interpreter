package Syntactic_Analyzer.ParseTree.Commands;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

public class IfCom extends Com {

	Exp b;
	Block c1, c2;

	/**
	 * Node of the Syntax Tree representing the conditional command.
	 * 
	 * @author Tommaso Catuogno
	 * @version 1.0
	 * @since 19/06/2015
	 */

	public IfCom(Exp in1, Block in2) {
		b = in1;
		c1 = in2;
		c2 = null;
		b.checkType("Bool");
	}

	public void addBranch(Block in) {
		c2 = in;

		if (c1.getType() != c2.getType())
			throw new TypeException(c1.getType(), c2.getType(), LINE);

	}

	@Override
	public Object eval() {
		if ((Boolean) b.eval())
			return c1.eval();
		else if (c2 != null)
			return c2.eval();
		return null;
	}

	@Override
	public String toString() {

		String i = "if " + b.toString() + " then " + c1.toString();
		if (c2 == null)
			return i;
		return i + " else " + c2.toString();
	}
}