package Syntactic_Analyzer.ParseTree.Commands;

import Syntactic_Analyzer.Parser;

/**
 * Node of the Syntax Tree representing a sequence of commands.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class ContCom extends Com {
	public ContCom() {
	}

	@Override
	public Object eval() {
		Parser.FLAG_CONTINUE = true;
		return null;
	}

	@Override
	public String toString() {
		return "continue;";
	}

}
