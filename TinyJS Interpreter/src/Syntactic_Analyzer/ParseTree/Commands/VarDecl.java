package Syntactic_Analyzer.ParseTree.Commands;

import Exceptions.SyntaxException;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Expression.ApplyFun;
import Syntactic_Analyzer.ParseTree.Expression.IDE;

/**
 * Node of the Syntax Tree representing the variable declaration statemenet.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class VarDecl extends Com {

	String n;
	Valuable a;
	String Type;

	public VarDecl(String in) {
		n = in;
		a = null;
	}

	public void addValue(Valuable in) {
		// Case for empty declaration.
		boolean NOT_INSTANCIATED = false;

		if (in instanceof ApplyFun && (in.getType() == "Undef"))
			throw new SyntaxException("" + ((ApplyFun) in).name
					+ " is a procedure and cannot be assigned", LINE);

		if (in == null) {
			Parser.symbol_table.setVal(n, new IDE(n, "Undef"));
			Type = "Undef";
		} else {
			a = in;
			// If already declared, error
			if (Parser.symbol_table.getVal(n) != null
					&& Parser.symbol_table.GetSymbType(n) != "Fun")
				throw new SyntaxException("Variable \"" + n
						+ "\" already declared in this scope", LINE);

			while (a instanceof IDE && !NOT_INSTANCIATED) {
				// Case for function variable in high order function
				if (Parser.symbol_table.getVal(a.toString()).toString() == a
						.toString())
					NOT_INSTANCIATED = true;
				else
					a = Parser.symbol_table.getVal(a.toString());
			}

			if (a.getType() == "Fun"
					&& Parser.symbol_table.getStaticLink() != -1
					&& !NOT_INSTANCIATED)
				throw new SyntaxException(
						"Nested function declaration not allowed.", LINE);
			Parser.symbol_table.setVal(n, a);
			Type = a.getType();
		}

	}

	@Override
	public String toString() {
		String ret = "var " + n;
		if (a != null)
			ret += " = " + a.toString();
		return ret + " ;";
	}

	public String getType() {
		return Type;

	}

	@Override
	public Object eval() {

		// Era cosi ma al momento non lo comprendo, probabile che vada
		// reintegrato

		if (Type != "Fun" || a instanceof IDE) {
			if (Type != "Undef")
				Parser.stack.setValue(n, a.eval()); 
			else
				Parser.stack.setValue(n, "Undef");
		} else
			Parser.stack.setValue(n, a);
		return null;
	}
}