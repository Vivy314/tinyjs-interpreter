package Syntactic_Analyzer.ParseTree.Commands;

import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing the for loop statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class ForCom extends Com {
	AssignCom a1, a3;
	VarDecl v1;
	Exp e;
	Block b;

	public ForCom(AssignCom in1, Exp in2, AssignCom in3, Block in4) {
		a1 = in1;
		e = in2;
		a3 = in3;
		b = in4;
		e.checkType("Bool");
	}

	public ForCom(VarDecl in1, Exp in2, AssignCom in3, Block in4) {
		v1 = in1;
		e = in2;
		a3 = in3;
		b = in4;
		e.checkType("Bool");
	}

	@Override
	public Object eval() {
		if (v1 == null)
			a1.eval();
		else
			v1.eval();

		while ((Boolean) e.eval()) {
			b.eval();
			if (Parser.FLAG_BREAK) {
				Parser.FLAG_BREAK = false;
				break;
			}
			if (Parser.FLAG_CONTINUE)
				Parser.FLAG_CONTINUE = false;
			a3.eval();
		}
		return null;
	}

	@Override
	public String toString() {
		if (v1 == null)
			return "for(" + a1.toString() + " ; " + e.toString() + " ; "
					+ a3.toString() + " ) " + b.toString();
		else
			return "for(" + v1.toString() + " ; " + e.toString() + " ; "
					+ a3.toString() + " ) " + b.toString();
	}
}
