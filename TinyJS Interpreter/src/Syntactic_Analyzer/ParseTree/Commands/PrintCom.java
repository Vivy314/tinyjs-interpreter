package Syntactic_Analyzer.ParseTree.Commands;

import java.util.Vector;

import Syntactic_Analyzer.ParseTree.FunExp;
import Syntactic_Analyzer.ParseTree.Valuable;
import Syntactic_Analyzer.ParseTree.Expression.IDE;

/**
 * Node of the Syntax Tree representing the print command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class PrintCom extends Com {
	Vector<Valuable> e;

	public PrintCom(Vector<Valuable> in) {
		e = in;

	}

	@Override
	public Object eval() {

		Valuable o;
		String ret = "";

		for (int i = 0; i < e.size(); i++) {

			o = e.get(i);

			if (o.eval() != null) {

				if (o.getType() == "Fun")
					if (o instanceof IDE)
						ret += o.eval().toString();
					else
						ret += ((FunExp) o).toString();
				else
					ret += e.get(i).eval().toString();
			}
			// Sure?
			else
				ret += "null";
		}
		System.out.println(ret);
		return null;
	}

	@Override
	public String toString() {
		String ret = "println(";
		for (int i = 0; i < e.size(); i++) {
			ret += e.get(i).toString();
			if (i < e.size() - 1)
				ret += ", ";
		}
		return ret + ");";
	}
}
