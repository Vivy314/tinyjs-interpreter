package Syntactic_Analyzer.ParseTree.Commands;

import Syntactic_Analyzer.Parser;

/**
 * Node of the Syntax Tree representing the break command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class BreakCom extends Com {
	public BreakCom() {
	}

	@Override
	public Object eval() {
		Parser.FLAG_BREAK = true;
		return null;
	}

	@Override
	public String toString() {
		return "break;";
	}

}
