package Syntactic_Analyzer.ParseTree.Commands;

import java.util.Vector;

import Exceptions.TypeException;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing the switch statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class SwitchCom extends Com {
	Exp input;
	Vector<Integer> cases;
	Vector<Block> bodies;
	Block default_;
	String ret_type = null;
	public boolean isdef = false;
	public boolean isdefcase = false;

	public SwitchCom(Exp in1) {
		cases = new Vector<Integer>();
		bodies = new Vector<Block>();
		input = in1;
		in1.checkType("Int");
	}

	@Override
	public Object eval() {
		boolean found = false;
		int i = 0;
		while ((i < cases.size()) && !found) {
			if (input.eval() == cases.get(i)) {
				bodies.get(i).eval();
				found = true;
			}
			i++;
		}
		if (!found)
			default_.eval();
		return null;
	}

	@Override
	public String toString() {
		String ret = "switch(" + input.toString() + "){";
		for (int i = 0; i < cases.size(); i++)
			ret += "\ncase(" + cases.get(i).toString() + "):"
					+ bodies.get(i).toString();
		ret += "\ndefault:" + default_.toString();
		ret += "\n}";
		return ret;
	}

	public void addCases(int in1, Block in2) {
		cases.add(in1);
		if (ret_type == null)
			in2.getType();
		else if (in2.getType() != ret_type)
			throw new TypeException(ret_type, in2.getType());
		bodies.add(in2);

		isdefcase = true;
	}

	public void addDefault(Block in) {
		default_ = in;
		if (ret_type == null)
			in.getType();
		else if (in.getType() != ret_type)
			throw new TypeException(ret_type, in.getType());

		isdef = true;
	}
}
