package Syntactic_Analyzer.ParseTree.Commands;

import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Block;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing a do while loop statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class DoCom extends Com {
	Block b;
	Exp e;

	public DoCom(Exp in1, Block in2) {
		e = in1;
		b = in2;
		e.checkType("Bool");

	}

	@Override
	public Object eval() {

		do {
			b.eval();
			if (Parser.FLAG_BREAK) {
				Parser.FLAG_BREAK = false;
				break;
			}
			if (Parser.FLAG_CONTINUE)
				Parser.FLAG_CONTINUE = false;
		} while ((Boolean) e.eval());

		return null;
	}

	@Override
	public String toString() {
		return "do " + b.toString() + " while( " + e.toString() + ")";
	}

}
