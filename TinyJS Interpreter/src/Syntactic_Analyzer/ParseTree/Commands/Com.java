package Syntactic_Analyzer.ParseTree.Commands;

import Lexical_Analyzer.Tokenizer;

/**
 * Node of the Syntax Tree representing a command.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public abstract class Com {
	int LINE = Tokenizer.LINE;

	public abstract Object eval();

	@Override
	public abstract String toString();

}
