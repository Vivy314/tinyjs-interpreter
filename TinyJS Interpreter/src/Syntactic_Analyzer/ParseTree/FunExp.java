package Syntactic_Analyzer.ParseTree;

import java.util.Vector;

import Exceptions.SyntaxException;
import Exceptions.TypeException;
import Syntactic_Analyzer.Parser;

/**
 * Node of the Syntax Tree representing the Function declaration.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */

public class FunExp extends Valuable {
	public Block body;
	public String name;
	public Vector<FormalParameter> arglist;
	public String rettype;
	public String Type = "Fun";
	public int SL;

	/**
	 * 
	 * @param in1
	 *            name of the function.
	 * @param in2
	 *            Formal parameter.
	 * @param in4
	 *            return type.
	 */
	public FunExp(String in1, Vector<FormalParameter> in2, String in4) {

		name = in1;
		arglist = in2;
		if (arglist == null)
			arglist = new Vector<FormalParameter>();
		rettype = in4;

			if (rettype == "Fun")
			throw new TypeException(
					"Functions cannot be returned as value, only passed as argument.",
					LINE);

		if (Parser.symbol_table.getVal(name) != null
				&& Parser.symbol_table.GetSymbType(name) == "Fun")
			throw new SyntaxException("Double declaration of function " + name
					+ " .", LINE);

		Parser.symbol_table.setVal(name, this);

	}

	/**
	 * @param in
	 *            the body of the function declaration.
	 */
	public void addBody(Block in) {
		body = in;
		if (Parser.symbol_table.getRetVal() == null) {
			if (rettype != "Undef")
				throw new SyntaxException(
						"Return value expected but not found", LINE);
		} else if (Parser.symbol_table.getRetVal().getType() != rettype)
			throw new TypeException(Parser.symbol_table.getRetVal().getType(),
					rettype);

		SL = Parser.symbol_table.getStaticLink();
		if (!Parser.FLAG_RET && rettype != "Undef")
			throw new SyntaxException("No value returned inside the function.",
					LINE);
		Parser.FLAG_RET = false;
	}

	/**
	 * return a string representing the actual object.
	 */
	@Override
	public String toString() {
		String ret = "Fun " + name + "(";
		if (arglist != null)
			for (int i = 0; i < arglist.size(); i++) {
				// if ( arglist.get(i).id != null ) ret= ret+ arglist.get(i).id;
				if (arglist.get(i) instanceof FunArg)
					ret += ((FunArg) arglist.get(i)).toString();
				else
					ret += ((SimpleArg) arglist.get(i)).toString();
			}

		ret += ")";
		if (rettype != "Undef")
			ret = ret + ":" + rettype;
		return ret + body.toString();
	}

	/**
	 * Return a string representing the type of the actual object.
	 */
	@Override
	public String getType() {
		return "Fun";
	}

	/**
	 * @param in
	 *            Type to be checked Check if the given type input match the
	 *            object's type. if it does not, throw an exception.
	 * @throws TypeException
	 *             \\
	 */
	@Override
	public void checkType(String in) {
		if (!in.equals("Fun"))
			throw new TypeException(in, "Fun");
	}

	@Override
	public Object eval() {
		Parser.stack.setValue(name, SL);

		return null;
	}
}