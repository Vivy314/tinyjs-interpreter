package Syntactic_Analyzer.ParseTree;

import Lexical_Analyzer.Tokenizer;

/**
 * Container class for object that can be evaluated.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public abstract class Valuable extends Object {
	public int LINE = Tokenizer.LINE;

	public abstract String getType();

	public abstract void checkType(String in);

	@Override
	public abstract String toString();

	public abstract Object eval();
}
