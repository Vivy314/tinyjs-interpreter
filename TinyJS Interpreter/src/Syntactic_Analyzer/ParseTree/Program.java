package Syntactic_Analyzer.ParseTree;

/**
 * Root of the Syntax Tree representing the Program.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public abstract class Program {
	/**
	 * Evaluate the code represented my the Syntax tree for which Program is the
	 * root.
	 * 
	 * @return program evaluation.
	 */
	public abstract Object eval();

	/**
	 * return a string representing the actual object.
	 * 
	 * @return String representing this object
	 */
	@Override
	public abstract String toString();
}
