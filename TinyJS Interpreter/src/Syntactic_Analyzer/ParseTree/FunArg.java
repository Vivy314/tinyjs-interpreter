package Syntactic_Analyzer.ParseTree;

import java.util.Vector;

import Exceptions.SyntaxException;
import Exceptions.TypeException;

public class FunArg extends FormalParameter {
	public String rettype;
	public Vector<FormalParameter> types;

	public FunArg(String in1, String in2, Vector<FormalParameter> tmp) {
		id = in1;
		rettype = in2;
		types = tmp;
	}

	/*
	 * public void checkSignature(FunExp in){ if ( in.rettype.equals(rettype))
	 * throw new TypeException("Formal parameter of function " + in.name +
	 * "does not match with the formal parameter." );
	 * 
	 * for( int k =0 ; k <types.size() ; k++ ) if
	 * (!types.get(k).equals(in.g.get(k))); throw new
	 * TypeException("Formal parameter of function " + in.name +
	 * "does not match with the formal parameter." );
	 * 
	 * }
	 */

	@Override
	public boolean equals(Object object) {
		if (object instanceof FormalParameter)
			if (((FormalParameter) object).id.equals(id))
				return true;
		return false;

	}

	@Override
	public void equal_type(FormalParameter in) {
		Vector<FormalParameter> tmp;
		if (in instanceof FunArg) {
			if (((FunArg) in).rettype != rettype)
				throw new TypeException(rettype, ((FunArg) in).rettype);
			tmp = ((FunArg) in).types;
		
			for (int i = 0; i < tmp.size(); i++)
				tmp.get(i).equal_type(this.types.get(i));
		} else
			throw new TypeException(((SimpleArg) in).types, "Fun");

	}

	public void equal_type(FunExp in) {
		Vector<FormalParameter> tmp = in.arglist;
		if (tmp.size() != this.types.size())
			throw new SyntaxException(
					"Wrong number of parameters when passing function as argument to function "
							+ in.name);
		if (rettype != in.rettype)
			throw new TypeException(rettype, in.rettype);

		for (int i = 0; i < tmp.size(); i++)
			types.get(i).equal_type(tmp.get(i));
	}

	@Override
	public String toString() {
		String ret = "";
		if (id != null)
			ret += id;
		ret += "(";
		for (int i = 0; i < types.size(); i++) {
			ret += types.get(i).toString();
			if (i < types.size() - 1)
				ret += ", ";
		}
		ret += "):" + rettype;
		return ret;
	}

}