package Syntactic_Analyzer.ParseTree;

public abstract class FormalParameter {
	public String id;
	public String types;

	// public abstract boolean equal_type(FormalParameter in);
	public abstract void equal_type(FormalParameter in);

	@Override
	public String toString() {
		if (this instanceof FunArg)
			return ((FunArg) this).toString();
		else
			return ((SimpleArg) this).toString();
	}

}
