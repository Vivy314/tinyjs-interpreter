package Syntactic_Analyzer.ParseTree;

import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Commands.Com;
import Syntactic_Analyzer.ParseTree.Expression.Exp;

/**
 * Node of the Syntax Tree representing a list of statement.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class StmtList extends Program {

	StmtList slist;
	Com par1;
	FunExp par2;
	Exp par3;

	public StmtList(Exp in) {
		par3 = in;

	}

	public StmtList(Com in) {
		par1 = in;
		par2 = null;
		slist = null;
	}

	public StmtList(FunExp in) {
		par2 = in;
		par1 = null;
		slist = null;
	}

	/**
	 * Concatenate a list of statement to the actual's one.
	 * 
	 * @param in
	 *            Statement list to be concatenated.
	 */
	public void addStmt(StmtList in) {
		slist = in;
	}

	/**
	 * Evaluate the code represented my the Syntax tree for which Program is the
	 * root.
	 * 
	 * @return program evaluation.
	 */

	@Override
	public Object eval() {
		Object ret = null;
		if (par1 != null)
			ret = par1.eval();
		if (par2 != null)
			ret = par2.eval();
		if (par3 != null)
			ret = par3.eval();
		if (slist != null && !Parser.FLAG_BREAK && !Parser.FLAG_CONTINUE
				&& !Parser.FLAG_RET)
			return slist.eval();
		return ret;

	}

	/**
	 * return a string representing the actual object.
	 * 
	 * @return String representing this object
	 */
	@Override
	public String toString() {
		String ret = "";
		if (par1 != null)
			ret = par1.toString();
		if (par2 != null)
			ret = par2.toString();
		if (par3 != null)
			ret = par3.toString();
		if (slist != null)
			return ret + "\n" + slist.toString();
		return ret;
	}
}