package Syntactic_Analyzer.ParseTree;

import Exceptions.TypeException;

public class SimpleArg extends FormalParameter {

	public SimpleArg(String in1, String in2) {
		id = in1;
		types = in2;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof FormalParameter)
			if (((FormalParameter) object).id.equals(id))
				return true;
		return false;
	}

	@Override
	public void equal_type(FormalParameter in) {

		if (!(in instanceof SimpleArg))
			throw new TypeException(types, "Fun");
		if (!((SimpleArg) in).types.equals(types))
			throw new TypeException(types, ((SimpleArg) in).types);

	}

	public void equal_type(String in) {
		if (!in.equals(types))
			throw new TypeException(types, in);
	}

	@Override
	public String toString() {
		String ret = "";
		if (id != null)
			ret = id + ":";
		return ret + types;
	}

}
