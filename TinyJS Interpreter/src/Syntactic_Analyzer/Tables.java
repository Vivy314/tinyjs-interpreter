package Syntactic_Analyzer;

import java.util.Hashtable;

import Syntactic_Analyzer.ParseTree.Valuable;

/**
 * Represent the record that are going to be used as element of the Symbol Table
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class Tables {
	public int StaticLink;
	public Valuable ret;
	Hashtable<String, Valuable> table;

	public Tables(int in2) {
		ret = null;
		table = new Hashtable<String, Valuable>();
		StaticLink = in2;
	}

	public int getStaticLink() {
		return StaticLink;
	}

	public void addSymb(String in, Valuable in2) {
		table.put(in, in2);
	}

}
