package Syntactic_Analyzer;

import static Syntactic_Analyzer.Command_parser.parseCom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import Exceptions.SyntaxException;
import Lexical_Analyzer.Tok;
import Lexical_Analyzer.Tokenizer;
import Syntactic_Analyzer.ParseTree.Program;
import Syntactic_Analyzer.ParseTree.StmtList;

/**
 * Core part implemeting the parser of the input code. It is the core part of
 * the Syntax analysis
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 *
 */
public class Parser {

	public static boolean FLAG_BREAK = false;
	public static boolean FLAG_LOOP = false;
	public static boolean FLAG_CONTINUE = false;
	public static boolean FLAG_FUN = false;
	public static boolean FLAG_RET = false;
	/**
	 * Stack used during the evaluation of the code at runtime.
	 */
	public static RunTimeStack stack = null;
	/**
	 * Table containing the link between symbols and types.
	 */
	public static SymbolTable symbol_table;
	private Program parsed;
	private Tokenizer tok;

	/**
	 * Class Constructor
	 * 
	 * @param in
	 *            Representation of the input code as a set of tokens.
	 */
	public Parser(Tokenizer in) {
		tok = in;
		tok.eat();
		symbol_table = new SymbolTable();
	}

	public Parser(int socket) {
		PrintWriter writer=null;
		ServerSocket serverSock=null;
		try {
			
			serverSock = new ServerSocket(socket);
			Socket sock = serverSock.accept();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));

			String in = reader.readLine();
			System.out.println("SERVER Leggo da socket!--> " + in);

			tok = new Tokenizer(in);
			tok.eat();
			symbol_table = new SymbolTable();

			this.Parse();

			Object res = this.Execute();

			System.out.println("SERVER scrivo su socket!--> " + res.toString());

			writer = new PrintWriter(sock.getOutputStream());
			writer.println(res.toString());
			writer.flush();
			writer.close();
			serverSock.close();
			tok = null;
			symbol_table = null;

		} catch (IOException e) {
			if (serverSock != null )
				try {
					serverSock.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			if (writer != null ) writer.close();
			e.printStackTrace();
		}
	}

	/**
	 * Class Constructor
	 * 
	 * @param in
	 *            Representation of the input code as a set of tokens.
	 * @param in2
	 *            Symbol Table to use while parsing.
	 */
	public Parser(Tokenizer in, SymbolTable in2) {

		tok = in;
		tok.eat();
		symbol_table = in2;
	}

	public void AddStack(RunTimeStack in) {
		stack = in;
	}

	/**
	 * Parse the input code, checking for Syntax problems and eventually
	 * generating the Syntax Tree
	 * 
	 * @return The root of the Syntax tree representing the parsed code is
	 *         returned.
	 */
	public Program Parse() {
		parsed = parseProgram();
		return parsed;
	}

	/**
	 * Execute method compute the evaluation of the Syntax tree.
	 * 
	 * @return the output value of the computation is returned.
	 */
	public Object Execute() {
		Object res = null;
		if (parsed == null) {
			System.out.println("Before evaluating che code need to be parsed");
			return null;
		}
		if (stack == null)
			stack = new RunTimeStack();
		res = parsed.eval();

		return res;
		/*
		 * OLD VERSION. try{ res = parsed.eval(); if ( res==null)
		 * System.out.println("Corrupted Syntax Tree");
		 * }catch(NullPointerException
		 * e){System.out.println("Corrupted Syntax Tree");}
		 */
	}

	private Program parseProgram() {
		StmtList stmt, morestmt;

		switch (tok.getToken().getFirst().getGroup()) {
		case COM:
			stmt = new StmtList(parseCom(tok));
			morestmt = parseStmtList();
			tok.eat(Tok.EOF);
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		case FUN:
			stmt = new StmtList(Command_parser.parseFunDecl(tok));
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			tok.eat(Tok.EOF);
			return stmt;

		case EXP:
			stmt = new StmtList(Expression_parser.parseExp(tok));
			tok.eat(Tok.S_COLUMN);
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			tok.eat(Tok.EOF);

			return stmt;

		case IDE:
			stmt = new StmtList(parseCom(tok));
			morestmt = parseStmtList();
			tok.eat(Tok.EOF);
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		default:
			throw new SyntaxException("Empty Program");

		}
	}

	private StmtList parseStmtList() {
		StmtList stmt, morestmt;
		switch (tok.getToken().getFirst().getGroup()) {
		case COM:
			stmt = new StmtList(parseCom(tok));
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		case FUN:
			stmt = new StmtList(Command_parser.parseFunDecl(tok));
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		case EXP:
			stmt = new StmtList(Expression_parser.parseExp(tok));
		
			tok.eat(Tok.S_COLUMN);
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		case IDE:

			tok.setBackup();
			tok.eat();
			if (tok.getToken().getFirst() != Tok.EQUAL) {
				tok.restore();
				stmt = new StmtList(Expression_parser.parseExp(tok));
				tok.eat(Tok.S_COLUMN);
			} else {
				tok.restore();
				stmt = new StmtList(Command_parser.parseCom(tok));
			}
			morestmt = parseStmtList();
			if (morestmt != null)
				stmt.addStmt(morestmt);
			return stmt;

		default:
			return null;
		}
	}
}