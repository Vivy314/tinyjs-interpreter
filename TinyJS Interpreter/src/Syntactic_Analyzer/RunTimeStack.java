package Syntactic_Analyzer;

import java.util.Hashtable;
import java.util.Vector;

import Lexical_Analyzer.Tokenizer;

// Required: 
// 			- Link to the enviroiment where we have to look for non local scope.
//			- Link to the activation record of the calling procedure
//			- 

/**
 * It's the structure implementing the stack required to interprete the code.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class RunTimeStack {
	private Vector<Activation_Record> stack;
	public int index;

	public RunTimeStack() {
		index = 0;
		stack = new Vector<Activation_Record>();
		stack.add(index, new Activation_Record(new Hashtable<String, Object>()));
	}

	/**
	 * 
	 * @param in
	 *            new Hashtable to be pushed in the stack
	 */
	public void pushStack(Hashtable<String, Object> in) {
		stack.add(++index, new Activation_Record(in));
	}

	/**
	 * delete from the stack the last SymbolTable added. Requires: At least one
	 * element in the stack
	 */
	public void popStack() {
		// System.out.println("** POPSTACK " + index);
		if (stack.get(index).retVal != null)
			this.setRetVal(stack.get(index).retVal);
		stack.remove(index--);
	}

	/**
	 * 
	 * @param in
	 *            Symbol to be added/modified in the actual SymbolTable
	 * @param in2
	 *            Value of be linked to the input symbol
	 */
	public void setValue(String in, Object in2) {
		if (in==null || in2 == null) System.out.println(in);
		stack.get(index).Values.put(in, in2);
	}

	/**
	 * 
	 * @param in
	 *            Symbol to be searched in the actual SymbolTable
	 * @return the value linked to the input symbol is returned.
	 */
	public Object getValue(String in) {
		Object tmp = stack.get(index).Values.get(in);
		if (tmp instanceof String && ((String) tmp).equals("Undef"))
			throw new RuntimeException("Inside call of function at Line <"
					+ Tokenizer.LINE + "> - Variable " + in
					+ " used inside function but not initialized");
		return tmp;
	}

	/**
	 * 
	 * @param in
	 *            Symbol to be added/modified in the most recent Activation
	 *            Record where the symbol occur
	 * @param in2
	 *            Value of be linked to the input symbol
	 */
	public void searchSet(String in, Object in2) {
		int i = index;
		while (i >= 0) {
			if (stack.get(i).Values.containsKey(in)) {
				stack.get(i).Values.put(in, in2);
				return;
			} else
				i--;

		}
	}

	/**
	 * 
	 * @param in
	 *            Symbol to be searched in the most recent Activation record
	 *            where the symbol occur
	 * @return the value linked to the input symbol is returned.
	 */
	public Object searchGet(String in, int line) {
		int i = index;

		while (i >= 0) {
			if (stack.get(i).Values.containsKey(in)) {
				Object tmp = stack.get(i).Values.get(in);
				if (tmp instanceof String && ((String) tmp).equals("Undef"))
					throw new RuntimeException(
							"Inside call of function at Line <"
									+ line
									+ "> - Variable "
									+ in
									+ " used inside function but not initialized");
				else
					return stack.get(i).Values.get(in);
			} else
				i--;
		}
		return null;
	}

	/**
	 * 
	 * @return The value that the actual stack will return
	 */
	public Object getRetVal() {

		// System.out.println("GETRETVAL>>> " + "  " + index + stack.get(index).retVal);
		return stack.get(index).retVal;
	}

	/**
	 * 
	 * @param in
	 *            Value to be set as reuturn of the actual stack.
	 */
	public void setRetVal(Object in) {
		stack.get(index - 1).retVal = in;
		// System.out.println("SETRETVAL>>> " + stack.get(index-1).retVal + "  "
		// + index);

	}

}