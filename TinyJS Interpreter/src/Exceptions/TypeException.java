package Exceptions;

import Lexical_Analyzer.Tokenizer;

/**
 * Exception representing a Type problem in the code.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class TypeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param type
	 *            Type found while the exception is thrown
	 * @param exptype
	 *            Expected type
	 */
	public TypeException(String type, String exptype) {
		if (type == "Undef")
			System.out.println("Type Checking failed - Line <" + Tokenizer.LINE
					+ ">: Variable used but not instanciated");
		else
			System.out.println("Type Checking failed - Line <" + Tokenizer.LINE
					+ ">: TYPE " + type + " found while TYPE " + exptype
					+ " was expected");

	}

	public TypeException(String string, int line) {
		System.out.println("Type Checking failed : " + string);
	}

	public TypeException(String type, String exptype, int line) {
		if (type == "Undef")
			System.out.println("Type Checking failed - Line <" + line
					+ ">: Variable used but not instanciated");
		else
			System.out.println("Type Checking failed - Line <" + line
					+ ">: TYPE " + type + " found while TYPE " + exptype
					+ " was expected");

	}

}