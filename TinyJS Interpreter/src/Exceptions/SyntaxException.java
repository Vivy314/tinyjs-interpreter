package Exceptions;

import Lexical_Analyzer.Token;
import Lexical_Analyzer.Tokenizer;

/**
 * Exception representing a Syntax problem in the code.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 *
 */
public class SyntaxException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param in
	 *            Actual token in the instant in which the syntax problem is
	 *            found.
	 */
	public SyntaxException(Token in) {
		System.out.println("Syntax Error occured at line " + in.line
				+ " : Unexpected identifier \"" + in.getSecond() + "\"");
	}

	/**
	 * @param in
	 *            String describing the Syntax problem occurred.
	 */
	public SyntaxException(String in) {
		System.out.println("Syntax Error occured at line <" + Tokenizer.LINE
				+ "> : " + in);
	}

	public SyntaxException(String in, int line) {
		System.out.println("Syntax Error occured at line <" + line + "> : "
				+ in);

	}

}
