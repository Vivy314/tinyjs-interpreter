package Lexical_Analyzer;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;
import java.util.regex.Pattern;

import Exceptions.SyntaxException;

/**
 * The Tokenizer class is the core of the Lexical analyzer part of the
 * interpreter and is used to break the input in Tokens
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class Tokenizer {
	public static int LINE = 1;
	private Scanner st;
	// private String regex =
	// "(\\#\\d\\#|[&]{2}|[|]{2}|[=]{1,2}|[a-zA-Z][a-zA-Z0-9_]{0,31}+|\\d+|[()\\[\\]+\\-*,/{};!]|\"[^\"]*\"|[^\\#^\\da-zA-Z()\\[\\]+\\-*=/{};!\\s]+)";
	private String regex = "(//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/|\\n|\"(([^\\\\\"]|\\\\\"|\\\\(?!\"))*)\"|[&]{2}|[|]{2}|[=]{1,2}|[a-zA-Z][a-zA-Z0-9_]{0,31}+|\\d+|[()\\[\\]+\\-*,/{};!]|[^\\#^\\da-zA-Z()\\[\\]+\\-*=/{};!\\s]+)";

	private int file_len;
	private Token t;
	private int Index, backup;
	private Vector<Token> tokenlist;

	/**
	 * class constructor
	 * 
	 * @param in
	 *            : Input file to be divided in tokens
	 */

	public Tokenizer(File in) throws IOException {

		Index = 0;
		@SuppressWarnings("resource")
		String content = new Scanner(in).useDelimiter("\\Z").next();

		// int count = content.split("\\n", -1).length;
		// content = content.replaceAll(
		// "//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "$1 " );
		st = new Scanner(content);

		st.reset();
		file_len = (int) in.length();

		tokenlist = new Vector<Token>();
		t = new Token(Tok.START, "");
		while (t.getFirst() != Tok.EOF) {
			t = NextTok();
			tokenlist.add(t);
		}
		tokenlist.add(new Token(Tok.EOF, ""));
	}

	/**
	 * class constructor
	 * 
	 * @param in
	 *            : Input String to be divided in tokens.
	 */

	public Tokenizer(String in) {
		st = new Scanner(in);
		file_len = in.length();

		tokenlist = new Vector<Token>();
		t = new Token(Tok.START, "");
		while (t.getFirst() != Tok.EOF) {
			t = NextTok();
			tokenlist.add(t);
		}
		tokenlist.add(new Token(Tok.EOF, ""));

		LINE = 1;
	}

	/**
	 * NextTok parse the code dividing the input in appropriate tokens for the
	 * languages that we are defining.
	 * 
	 * @return The one token following the actual state is returned.
	 * @throws SyntaxException
	 */

	public Token NextTok() {
		/**
		 * @param token
		 *            used to split
		 */
		String token = st.findWithinHorizon(Pattern.compile(regex), file_len);
		// test if Tok is numeric and in case convert and return Tok.NUM
		if (token == null)
			return new Token(Tok.EOF, "");
		 if (token.equals( "\""  )) throw new SyntaxException(token
					+ " is not a valid chars sequence", LINE);
		String newline = System.getProperty("line.separator");

		if (token.contains(newline) && (token.length() == 1)) {
			Tokenizer.LINE++;
			return NextTok();
		}

		try {
			int x = Integer.parseInt(token);
			return new Token(Tok.NUM, x);
		} catch (Exception NumberFormatException) {

			switch (token) {
			// Commands token
			case "var":
				return new Token(Tok.VAR, "");
			case "if":
				return new Token(Tok.IF, "");
			case "else":
				return new Token(Tok.ELSE, "");
			case "function":
				return new Token(Tok.FUNCTION, "");
			case "for":
				return new Token(Tok.FOR, "");
			case "while":
				return new Token(Tok.WHILE, "");
			case "break":
				return new Token(Tok.BREAK, "");
			case "continue":
				return new Token(Tok.CONTINUE, "");
			case "switch":
				return new Token(Tok.SWITCH, "");
			case "case":
				return new Token(Tok.CASE, "");
			case "default":
				return new Token(Tok.DEFAULT, "");
			case "eval":
				return new Token(Tok.EVAL, "local");
			case "distribute_eval":
				return new Token(Tok.EVAL, "distributed");
			case "return":
				return new Token(Tok.RETURN, "");
			case "do":
				return new Token(Tok.DO, "");
			case "println":
				return new Token(Tok.PRINT, "");

				// Brackets
			case "(":
				return new Token(Tok.O_R_BRACKET, "(");
			case ")":
				return new Token(Tok.C_R_BRACKET, ")");
			case "{":
				return new Token(Tok.O_C_BRACKET, "}");
			case "}":
				return new Token(Tok.C_C_BRACKET, "{");
				// Expressions operand
			case "||":
				return new Token(Tok.OR, "");
			case "&&":
				return new Token(Tok.AND, "");
			case "!":
				return new Token(Tok.NOT, "");
			case "+":
				return new Token(Tok.PLUS, "+");
			case "-":
				return new Token(Tok.MINUS, "-");
			case "/":
				return new Token(Tok.DIV, "/");
			case "*":
				return new Token(Tok.MUL, "*");
			case "==":
				return new Token(Tok.D_EQUAL, "==");
			case "=":
				return new Token(Tok.EQUAL, "=");
			case "<":
				return new Token(Tok.LESS, "<");
			case ">":
				return new Token(Tok.GREATER, ">");

				// Types
			case "int":
				return new Token(Tok.TYPE, "Int");
			case "string":
				return new Token(Tok.TYPE, "String");
			case "bool":
				return new Token(Tok.TYPE, "Bool");
			case "fun":
				return new Token(Tok.TYPE, "Fun");
			case ";":
				return new Token(Tok.S_COLUMN, ";");
			case ",":
				return new Token(Tok.COMMA, ",");
			case ":":
				return new Token(Tok.COLUMN, ":");
			case "true":
				return new Token(Tok.BOOL, "true");
			case "false":
				return new Token(Tok.BOOL, "false");
			default:
				if (token.startsWith("\"")) {
					token = token.replace("\\\"", "\"");

					LINE += token.length()
							- token.replaceAll("\n", "").length();

					return new Token(Tok.STRING, token);
				}

				if (token
						.matches("//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/")) {

					int count = token.split("\\n", -1).length;
					if (count > 1)
						LINE += count - 1;
					// System.out.println("000000000000000000000000 " +
					// token.split("\\n", -1).length);
					token.replaceAll(
							"//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/",
							"$1 ");
					return NextTok();
				}

				if (token.matches("[a-zA-Z][a-zA-Z0-9_]{0,31}"))
					return new Token(Tok.IDE, token);

				throw new SyntaxException(token
						+ " is not a valid chars sequence", LINE);

			}
		}

	}

	/**
	 * Eat and advance the pointer skipping the actual token.
	 */

	public void eat() {
		if (Index < tokenlist.size())
			t = tokenlist.get(Index);
		LINE = t.line;
		Index++;
	}

	/**
	 * Get the value of the last token read.
	 * 
	 * @return The actual token value is returned
	 */

	public Token getToken() {
		return t;
	}

	/**
	 * Eat advance the pointer skipping the actual token and checking that the
	 * actual token was the expected one.
	 * 
	 * @param a
	 *            Expected token compared against the actual one.
	 * @throws SyntaxException
	 */

	public void eat(Tok a) {

		if (this.getToken().getFirst() != a)
			throw new SyntaxException(this.getToken());
		eat();
	}

	/**
	 * CheckToken Check that the actual token is the expected one.
	 * 
	 * @param a
	 *            Expected token compared against the actual one.
	 * @throws SyntaxException
	 */

	public void checkToken(Tok a) {
		if (this.getToken().getFirst() != a)
			throw new SyntaxException(this.getToken());
	}

	/**
	 * setBackup save the actual state of the tokenizer.
	 */

	public void setBackup() {
		backup = Index - 1;
		LINE = t.line;
	}

	/**
	 * restore the past tokenizer state saved by the last invocation of
	 * setBackup. Require: setBackup previously called.
	 */
	public void restore() {
		Index = backup;
		t = tokenlist.get(Index++);
		LINE = t.line;
	}

	/**
	 * toString print the representation of the tokenizer by listing the tokens
	 * that compose the input.
	 */
	@Override
	public String toString() {
		return tokenlist.toString();
	}
}