package Lexical_Analyzer;

/**
 * Class representing a token
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public class Token {
	private Tok val1;
	private Object val2;
	public int line;

	/**
	 * 
	 * @param a
	 *            Token Type
	 * @param b
	 *            Token Value
	 */
	public Token(Tok a, Object b) {
		line = Tokenizer.LINE;
		val1 = a;
		val2 = b;
	}

	/**
	 * get the token type.
	 * 
	 * @return the Type of the token is returned.
	 */
	public Tok getFirst() {
		return val1;
	}

	/**
	 * get the token value.
	 * 
	 * @return the value of the token is returned.
	 */
	public Object getSecond() {
		return val2;
	}

	public int getLine() {
		return line;
	}

	/**
	 * set the token type.
	 * 
	 * @param a
	 *            token type to be set.
	 */
	public void setFirst(Tok a) {
		val1 = a;
	}

	/**
	 * set the token value.
	 * 
	 * @param a
	 *            token value to be set.
	 */
	public void setSecond(Object a) {
		val2 = a;
	}

	@Override
	public String toString() {
		return "( " + val1.toString() + " - " + val2.toString() + " )";
	}
}
