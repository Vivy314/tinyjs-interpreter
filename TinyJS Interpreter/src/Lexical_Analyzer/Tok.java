package Lexical_Analyzer;

/**
 * Tok describe the set of possibles tokens of in which the input program will
 * be divided.
 * 
 * @author Tommaso Catuogno
 * @version 1.0
 * @since 19/06/2015
 */
public enum Tok {
	START, EOF, CASE, DEFAULT, EVAL(Group.EXP), DISTRIBUTED(Group.EXP),

	PRINT(Group.COM), IF(Group.COM), ELSE, OP, FOR(Group.COM), WHILE(Group.COM), DO(
			Group.COM), BREAK(Group.COM), CONTINUE(Group.COM), RETURN(Group.COM), FUNCTION(
			Group.FUN), COMMA, COLUMN, VAR(Group.COM), SWITCH(Group.COM),

	NUM(Group.EXP), BOOL(Group.EXP), STRING(Group.EXP),

	EQUAL, NOTEQ, PLUS, MINUS(Group.EXP), DIV, MUL, OR, AND, D_EQUAL, NOTEQUAL, LESS, GREATER, LESSEQ, GREATEREQ, O_R_BRACKET(
			Group.EXP), C_R_BRACKET, O_C_BRACKET, C_C_BRACKET, TRUE, FALSE, NOT(
			Group.EXP), S_COLUMN, TYPE, IDE(Group.IDE);

	private Group group;

	/**
	 * Constructor
	 * 
	 * @param group
	 *            Specify at which group the declared token belong.
	 */

	Tok(Group group) {
		this.group = group;
	}

	/**
	 * Constructor with undefined group.
	 */
	Tok() {
		this.group = Group.UNDEF;
	}

	/*
	 * @return return the group at which the token belong.
	 */
	public Group getGroup() {
		return this.group;
	}

	/**
	 * 
	 * Group enumeration, 5 different group exist:
	 * <p>
	 * <b> COM </b>: Command token
	 * <p>
	 * <b> EXP </b>: Expression token
	 * <p>
	 * <b> FUN </b>: Function token
	 * <p>
	 * <b> IDE </b>: Identifier token
	 * <p>
	 * <b> UNDEF </b>: Undefined token
	 */
	public enum Group {
		COM, EXP, FUN, IDE, UNDEF;
	}
}
