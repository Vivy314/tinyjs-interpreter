package Tests;

import java.io.File;
import java.io.IOException;

import Lexical_Analyzer.Tokenizer;
import Syntactic_Analyzer.Parser;
import Syntactic_Analyzer.ParseTree.Program;

public class Main {

	public static void main(String[] args) throws IOException {
		if (args.length == 0) {
			System.out.println("Insert as argument a text file containing the code to be evaluated");
			return;
		}
		File in = new File(args[0]);
		Tokenizer a = new Tokenizer(in);
		Parser b = new Parser(a);
		Program exp = b.Parse();

		if (exp == null)
			System.out.println("Null returned");
		else {
			System.out.println("\nCode Compilated:\n\n"
					+ exp.toString() + "\n\n");
			System.out
			.println("Evaluation:\n");

			
			b.Execute();

		}

	}
}